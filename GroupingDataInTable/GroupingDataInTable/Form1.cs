﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroupingDataInTable
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            loadDefaultData();
        }

        private void loadDefaultData()
        {
            var connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=ShipmentRegistrationDB;User id = testUser;password=qwert";
            using (var connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("SELECT * FROM ShipmentRegistration", connection))
                {
                    command.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(command))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            dataGridView1.DataSource = dt;
                        }
                    }
                }
            }
        }

        public void exampleMethod()
        {
            // connect to DB
            // make sql request with grouping by column which header was clicked
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // write sql request with grouping by hardcoded column name
            loadDefaultData();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=ShipmentRegistrationDB;User id = testUser;password=qwert";
            using (var connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("SELECT * FROM ShipmentRegistration WHERE City = 'Devin'", connection))
                {
                    command.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(command))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            dataGridView1.DataSource = dt;
                        }
                    }
                }
            }
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {

        }
    }
}
